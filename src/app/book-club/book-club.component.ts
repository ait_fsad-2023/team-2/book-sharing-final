import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-book-club',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './book-club.component.html',
  styleUrl: './book-club.component.css'
})
export class BookClubComponent {

}
