import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-register-book',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './register-book.component.html',
  styleUrl: './register-book.component.css'
})
export class RegisterBookComponent {

}
