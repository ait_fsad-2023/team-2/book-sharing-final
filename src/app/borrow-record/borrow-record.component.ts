import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-borrow-record',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './borrow-record.component.html',
  styleUrl: './borrow-record.component.css'
})
export class BorrowRecordComponent {

}
