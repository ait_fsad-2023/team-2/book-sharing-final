const express = require("express");
const path = require("path");

const app = express();

// Set Content Security Policy
app.use((req, res, next) => {
  res.setHeader("Content-Security-Policy", "style-src 'self' 'unsafe-inline';");
  return next();
});

// Serve only the static files from the dist directory
app.use(express.static(__dirname + "/dist/book-sharing-final"));

app.get("/*", function (req, res) {
  res.sendFile(path.join(__dirname + "/dist/book-sharing-final/browser/index.html"));
});

// Start the app by listening on the default Heroku port
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
